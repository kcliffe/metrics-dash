#!env/bin/python

"""
This guy is currently hard coded as a POC but it could
simply act as a controller - fetch from an input, stash to an output
but then it starts to look like LogStash.....
"""

import requests
import yaml
import schedule
import time
import platform
import psutil
import json
import re
from metricStasherElastic import MetricStasherElastic


def gather_node_stats():
    """
    Use psutil to gather very basic node stats which are annoying to get / interpret via JMX
    """
    print 'node_stats'
    # easy to get stats
    # but need to package for consistency
    # format seems to be:
    #   node
    #   timestamp
    #   value : {} OR
    #   valueIs
    print "gathering node metrics..."

    # cpu
    cpu_usage = psutil.cpu_percent(interval=None) # may return 0 on first call?
    metric_stasher_elastic.stash('cpu_usage', new_wrapper(cpu_usage))

    # memory
    memory_usage = psutil.virtual_memory()
    metric_stasher_elastic.stash('memory_usage', new_wrapper(memory_usage.__dict__))

    # disk stats
    disk_stats = psutil.disk_io_counters(True)  # per disk controller
    for key in disk_stats:
        vals = disk_stats[key].__dict__
        vals['disk'] = key
        metric_stasher_elastic.stash('disk_io', new_wrapper(vals))

    # disk usage
    # TODO -> enumerate mounts and call per mount :-(
    disk_usage = psutil.disk_usage('/')
    metric_stasher_elastic.stash('disk_usage', new_wrapper(disk_usage.percent))


def gather_cassandra_stats():
    """
    Use JMX (Http)
    """
    print "gathering cassandra metrics..."
    for metric in conf['cassandra']['metrics']:
        print "stashing metric %s" % metric['metric']['type']
        stash_metrics(metric, read_jmx_metric(metric))


def gather_elastic_stats():
    """
    Use Elastic client + uri to gather ES
    """
    print "gathering elastic metrics..."
    seconds_since_epoch = int(time.time())

    # standard ES api calls
    for metric in conf['elastic']['metrics']:
        url = conf['es']['host'] + metric['metric']['url']
        response = requests.get(url)

        print "querying %s" % url
        print "stashing metric %s" % (metric['metric']['type'])
        stash_metrics(metric, new_wrapper(json.loads(response.content)))


def gather_elastic_cat_stats():
    """
    Gather ES stats using CAT API (easier to grep results)
    """
    print "gathering elastic metrics..."
    seconds_since_epoch = int(time.time())

    # _cat API calls (simpler to extract)
    for metric in conf['elastic']['cat-metrics']['metrics']:
        url = conf['es']['host'] + metric['metric']['url']
        # add headers from config
        url = url + '?h=' + metric['metric']['headers']
        response = requests.get(url).content
        cols = metric['metric']['columns'].split(',')
        values = response.split(' ')
        clean_values = []

        valid = [v for v in values if v != "\n"]
        for v in valid:
            # can return formats like "56Mb"
            clean_values.append(clean_non_alpha(v))

        print "stashing metric %s" % (metric['metric']['type'])
        stash_metrics(metric, new_wrapper(dict(zip(cols, clean_values))))


def new_wrapper(val):
    """
    Create a uniform representation of the metric
    """
    # convert to milliseconds since epoch since ES 1.7.0 can't handle seconds since epoch mapping
    seconds_since_epoch = int(time.time()) * 1000
    return {
        "timestamp": seconds_since_epoch,
        "value": val,
        "host": conf['host']
    }


def read_jmx_metric(metric):
    """
    read metrics via http via jolokia
    """
    abs_url = make_absolute_metric_url(metric['metric']['url'])
    result = requests.get(abs_url)
    return result.json()


def stash_metrics(metric, result):
    """
    persist metrics
    """
    metric = metric['metric']
    # add host
    result['host'] = conf['host']
    metric_stasher_elastic.stash(metric['type'], result)


def load_config():
    """
    Config related
    """
    with open("conf.yaml", 'r') as stream:
        try:
            conf = yaml.load(stream)
            conf['host'] = platform.node()

            return conf
        except yaml.YAMLError as exc:
            print(exc)
            raise


def init_from_config(conf_cass):
    # todo -> refactor so we don't need to pass enpty args
    global metric_stasher_elastic, has_cass_mets, has_node_mets, has_es_mets, has_es_cat_mets
    # TODO. From args
    ver = "1.7.0"
    metric_stasher_elastic = MetricStasherElastic(conf['es']['host'], conf['es']['index-name'], ver)

    # make sure all types have mappings in Elasticsearch.
    # we're currently only really setting up the timestamp which cassandra emits in epoch-second
    if 'metrics' in conf['cassandra']:
        has_cass_mets = True
        conf_cass = conf['cassandra']
        for metric in conf_cass['metrics']:
            metric_stasher_elastic.init_metric_type_mapping(metric['metric']['type'])

    if 'metrics' in conf['elastic']:
        has_es_mets = True
        for metric in conf['elastic']['metrics']:
            metric_stasher_elastic.init_metric_type_mapping(metric['metric']['type'])

    if 'metrics' in conf['elastic']['cat-metrics']:
        has_es_cat_mets = True
        for metric in conf['elastic']['cat-metrics']['metrics']:
            metric_stasher_elastic.init_metric_type_mapping(metric['metric']['type'])

    if 'metrics' in conf['node_stats']:
        has_node_mets = True
        for metric in conf['node_stats']['metrics']:
            metric_stasher_elastic.init_metric_type_mapping(metric)


def make_absolute_metric_url(url):
    return conf['cassandra']['jolokia-root'] + url


def clean_non_alpha(str):
    non_decimal = re.compile(r'[^\d.]+')
    clean = non_decimal.sub('', str)
    return float(clean)


if __name__ == "__main__":
    has_cass_mets = False
    has_node_mets = False
    has_es_mets = False
    has_es_cat_mets = False

    conf = load_config()
    init_from_config(conf)

    interval = int(conf['interval'])

    if has_cass_mets:
        schedule.every(interval).seconds.do(gather_cassandra_stats)
        gather_cassandra_stats()

    if has_node_mets:
        schedule.every(interval).seconds.do(gather_node_stats)
        gather_node_stats()

    if has_es_mets:
        schedule.every(interval).seconds.do(gather_elastic_stats)
        gather_elastic_stats()

    if has_es_cat_mets:
        cat_interval = conf['elastic']['cat-metrics']['interval']
        schedule.every(interval).seconds.do(gather_elastic_cat_stats)
        gather_elastic_cat_stats()
        print "Host es-cat metrics every %d seconds" % cat_interval

    print "Host %s gathering metrics every %d seconds" % ( conf['host'], interval)
    # kick off initial collections

    while True:
        schedule.run_pending()
        time.sleep(4)

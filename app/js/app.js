$(document).ready(function() {

	var chartsCount = 0;

    /*
        Create UI element to show graph
    */
	function createGraphContainer(name, title) {
        var div = $('<div class="graphgrid-item"><div class="graphgrid-title">' + title +'</div><div class="content">123</div></div>');
        var el = $('#graphgrid').append(div);
		return div.children(".content")[0];
	}

    /*
        Create a chart showing ... chartData
    */
	createChart = function(chartData) {

		return new Highcharts.Chart({
			chart: {
				renderTo: createGraphContainer(chartData.name, chartData.title),
				animation: false,
				defaultSeriesType: 'line',
				shadow: false
			},
			title: { text: null },
			xAxis: { type: 'datetime' },
			yAxis: {
				title: { text: chartData.title }
			},
			legend: {
				enabled: true,
				borderWidth: 0
			},
			credits: {enabled: false},
			exporting: { enabled: false },
			plotOptions: {
				area: {
					marker: {
						enabled: false
					}
				}
			},
			series: chartData.series
		})
	}

    /*
        Retrieve metrics of a specific type from Elastic
    */
	var getMetrics = function (metType) {
	    //var url = getParameterByName('eshost') ?? 'http://localhost';

		$.ajax({
  		  type: "POST",
		  dataType: "json",
		  url: 'http://localhost:9200/cassandra_metrics/' + metType.type + '/_search',
		  data: '{}',
		  success: function(data) {
            var graphSeries = [];
            metType.stats.forEach(function(stat) {
                // map the returned "values" which are contained in some arbitrary schema
                // to a single value ready for adding to a graph
                var seriesVals = $.map(data.hits.hits, function(el) {
                    var val = valueFromString(stat.path, el._source);
                    return val;
                });

                var s = {
                   name: stat.label,
                   data: seriesVals
                };

                if (stat.codeRed) {
                    s.color = '#FF0000';
                }

                // create the HighCharts specific data series
                graphSeries.push(s);

            });

            if (metType.debug) {
                console.log('Type:' + metType.type + ' series: ' + JSON.stringify(graphSeries));
            }

            // create the chart
		  	createChart({
		  		name: metType.type,
		  		title: metType.title ? metType.title : metType.type,
		  		series: graphSeries
		  	});
		  }
		});
	}

    /*
        Provide a method to extract a value from an arbitrary property on a JS object
    */
	var valueFromString = function(str, obj) {
        return str.split('.').reduce(index, obj);
	}

	var index = function (obj,i) {
	    return obj[i]
	}

	/*
	    Does what it says
	*/
	var getParameterByName = function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

	var cassandra_mets = [
		{ type: 'read_latency',
          stats: [ { path: 'value.Mean', label: 'Mean'} ]
		},
		{ type: 'write_latency',
		  stats: [ { path:'value.Mean', label: 'Mean'} ]
		},
		{ type: 'disk_usage',
		  stats: [ { path: 'valueIs', label: 'Usage %'} ]
		},
		{ type: 'compactions_pending',
		  stats: [ { path: 'valueIs', label: 'Compactions'} ]
		}];

    var node_mets = [
		{ type: 'cpu_usage',
		title: 'CPU Usage',
		  stats: [ { path: 'valueIs', label: 'Usage %'} ]
		},
		{ type: 'memory_usage',
		title: 'Memory Usage',
		  stats: [
		   { path: 'value.total', label: 'Total bytes'},
		   { path: 'value.available', label: 'Available bytes'}]
		} /*,
		{ type: 'disk_io',
		title: 'Disk I/O',
		  stats: [ { path: 'value.read_count', label: 'Read count'},
		   { path: 'value.write_count', label: 'Write count'},
		   { path: 'value.read_bytes', label: 'Read bytes'},
		   { path: 'value.write_bytes', label: 'Write bytes'}]
		}*/
    ];

    /*
    Can't be graphed with other mem stat - needs it's own graph
    { path: 'value.percent', label: '% Used'}
    */

	var es_mets = [
		{ type: 'es_health',
		  debug: true,
		  title: 'Health',
          stats: [ { path: 'value.active_primary_shards', label: 'Primary shards'},
          { path: 'value.number_of_nodes', label: 'Number of nodes'},
          { path: 'value.unassigned_shards', label: 'Unassigned shards', codeRed: true},
           { path: 'value.initializing_shards', label: 'Primary shards'}]
		},
		{
		    type: 'es_thread_pool',
		    title: 'Thread Pool',
            stats: [ { path: 'value.bulk-active', label: 'bulk active' },
                   { path: 'value.bulk-queued', label: 'bulk queued' },
                    { path: 'value.bulk-rejected', label: 'bulk rejected', codeRed: true }]
		},
		{ type: 'es_heap',
		title: 'Heap',
		  debug: true,
          stats: [ { path: 'value.heap-current', label: 'heap current' },
                   { path: 'value.heap-percent', label: 'heap percent' },
                   { path: 'value.heap-max', label: 'heap max' }]
		},
		{ type: 'es_indexing_stats',
		title: 'Indexing Stats',
          stats: [ { path: 'value.indexing-current', label: 'indexing current' },
                   { path: 'value.indexing-time', label: 'indexing time' },
                   { path: 'value.indexing-total', label: 'indexing total' }]
		},
		{ type: 'es_merges',
		title: 'Merges',
          stats: [ { path: 'value.merges-current', label: 'merges current' },
                   { path: 'value.merges-current-docs', label: 'current docs merged' },
                   { path: 'value.merges-total-merges', label: 'total merges' },
                   { path: 'value.merges-total-docs', label: 'total docs merged' },
                   { path: 'value.merges-total-size', label: 'total size merged' }]
		}];

	$('#graphgrid').masonry({
      itemSelector: '.graphgrid-item',
      columnWidth: 400
    });

	/*cassandra_mets.forEach(function(met) {
		// createNewPortlet(met.type);
		getMetrics(met);
	});*/

	es_mets.forEach(function(met) {
		// createNewPortlet(met.type);
		getMetrics(met);
	});

	node_mets.forEach(function(met) {
		// createNewPortlet(met.type);
		getMetrics(met);
	});
});
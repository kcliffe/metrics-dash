import pkg_resources

class MetricStasherElastic:
    def __init__(self, elastic_base_url, metric_index_name, ver):
        pkg_resources.require("elasticsearch==%s" % ver)
        import elasticsearch as es

        self.ver = ver
        self.elastic_base_url = "http://localhost:9200" if elastic_base_url == "" else elastic_base_url

        try:
            self.client = es.Elasticsearch()
            self.metric_index_name = metric_index_name

            # create index if it doesn't exists
            self.client.indices.create(metric_index_name, ignore=400) 
        except es.Elasticsearch.exceptions.ConnectionError:
            print "Cannot connect to Elastic on %s... is it running?" % self.elastic_base_url
            #raise


    def init_metric_type_mapping(self, type_name):
        self.client.indices.put_mapping(index=self.metric_index_name, doc_type=type_name, body={
            "properties": {
                "timestamp": {
                    "type": "date"
                }
            }
        })


    def stash(self, type_name, metrics):
        # elastic will see documents where "value" is a complex object
        # (e.g read_latency) and some that arent (e.g. cpu_load)
        # and even though they're across types it spits
        if not isinstance(metrics['value'], dict):
            metrics['valueIs'] = metrics['value']
            del metrics['value']

        # todo TTL (1 week?)
        self.client.index(index=self.metric_index_name, doc_type=type_name, body=metrics)

